
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Create Flight</title>
</head>
<body>
<form action="FlightServlet" method="post">
    Flight number<input type="text" name="flightNumber" value="932"> <BR>
    Airplane Type: <input type="text" name="airplaneType" value="boeing"> <BR>
    Departure City : <input type="text" name="departureCity" value="1"> <BR>
    Departure Date : <input type="text" name="departureDate" value="2016-10-10 10:10:10"> <BR>
    Arrival City : <input type="text" name="arrivalCity" value="2"> <BR>
    Arrival Date : <input type="text" name="arrivalDate" value="2016-10-10 10:10:10"> <BR>
    <input type="hidden" name="action" value="create"/>
    <input type="submit" />
</form>
</body>
</html>
