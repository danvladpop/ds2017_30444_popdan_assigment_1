package assign1;


import assign1.service.TimeStampService;

import java.sql.Timestamp;

public class Main {
    public static void main(String[] args) {

        TimeStampService timeStampService = new TimeStampService();
        Timestamp timestamp = timeStampService.getTimeStampForCoordinates(-3.70379f, 40.416775f);
        System.out.println(timestamp);
        return;
    }
}
