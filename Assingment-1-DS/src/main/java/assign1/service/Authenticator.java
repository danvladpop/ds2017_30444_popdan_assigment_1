package assign1.service;

import org.hibernate.cfg.Configuration;
import assign1.model.User;


public class Authenticator {
    public String authenticate(String username, String password){
        UserService userService = new UserService(new Configuration().configure().buildSessionFactory());
        User user = userService.getUserByName(username);
        if(user==null)
            return "false";
        else if(user.getUsername().equals("admin") && user.getPassword().equals("admin")){
            return "admin";
        }
        else {
            if(user.getPassword().equals(password)){
                return "user";
            }

        }
        return "false";
    }
}
