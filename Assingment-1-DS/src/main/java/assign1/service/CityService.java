package assign1.service;

import org.hibernate.*;
import assign1.model.City;

import java.util.List;


public class CityService{
    private SessionFactory factory;

    public CityService(SessionFactory factory) {
        this.factory = factory;
    }

    public City add(City c) {
        City city = c;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(city);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return city == null || session == null ? null : city;
    }

    public City delete(City c) {
        City city = c;

        if (getCity(city.getId()) != null) {
            Session session = factory.openSession();
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                session.delete(city);
                tx.commit();
            } catch (HibernateException e) {
                if (tx != null) {
                    tx.rollback();
                }
            } finally {
                session.close();
            }
        }
        return city == null ? null : city;
    }

    public City edit(City c) {
        City city = c;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(city);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return city == null || session == null ? null : city;
    }

    public City getCity(int id) {
        List<City> citys = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE id = :id");
            query.setParameter("id", id);
            citys = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return citys == null || session == null ? null : citys.get(0);
    }
}
