package assign1.service;

import org.hibernate.*;
import assign1.model.Flight;
import java.util.List;


public class FlightService{

    private SessionFactory factory;

    public FlightService(SessionFactory factory) {
        this.factory = factory;
    }

    public Flight add(Flight f) {
        Flight flight =  f;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return flight == null || session == null ? null : flight;
    }

    public Flight delete(Flight f) {
        Flight flight = f;

        if (getFlight(flight.getId()) != null) {
            Session session = factory.openSession();
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                session.delete(flight);
                tx.commit();
            } catch (HibernateException e) {
                if (tx != null) {
                    tx.rollback();
                }
            } finally {
                session.close();
            }
        }
        return flight == null ? null : flight;
    }

    public Flight edit(Flight f) {
        Flight flight = f;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return flight == null || session == null ? null : flight;
    }

    public Flight getFlight(int id) {
        List<Flight> flights = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE id = :id");
            query.setParameter("id", id);
            flights = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return flights == null || session == null ? null : flights.get(0);
    }

    public List<Flight> getAllFlights(){
        List<Flight> flights = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight");
//            query.setParameter("id", id);
            flights = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return flights == null || session == null ? null : flights;
    }
}
