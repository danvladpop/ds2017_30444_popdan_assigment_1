package assign1.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.sql.Timestamp;


public class TimeStampService {
    public Timestamp getTimeStampForCoordinates(float longitude, float latitude) {
        try {
            Timestamp currentDate = new Timestamp(System.currentTimeMillis());
            long currentTime = currentDate.getTime() / 1000 + currentDate.getTimezoneOffset() * 60;
            String Key = "AIzaSyCu23VbfJjhx970-nRTJIIqkUbsXSfhxmc";
            String url = "https://maps.googleapis.com/maps/api/timezone/json";
            HttpResponse<JsonNode> jsonResponse = Unirest.get(url)
                    .header("accept", "application/json")
                    .queryString("location", longitude + "," + latitude)
                    .queryString("timestamp", currentTime)
//                    .queryString("key", Key)
                    .asJson();
            Integer rawOffset = (Integer) jsonResponse.getBody().getObject().get("rawOffset");
            Integer dstOffset = (Integer) jsonResponse.getBody().getObject().get("dstOffset");
            Integer offset = dstOffset * 1000 + rawOffset * 1000;
            Timestamp localTime = new Timestamp(currentTime * 1000 + offset);
            return localTime;
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }
}
