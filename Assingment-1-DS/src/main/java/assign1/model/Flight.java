package assign1.model;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Table(name = "flight", catalog = "flightDB")
public class Flight implements java.io.Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;
    @Column(name = "flightNumber", unique = true, nullable = false)
    private String flightNumber;
    @Column(name = "airplaneType", nullable = false)
    private String airplaneType;
    @Column(name = "departureCityId", nullable = false)
    private int departureCityId;
    @Column(name = "departureDateTime", nullable = false)
    private Timestamp departureDateTime;
    @Column(name = "arrivalCityId", nullable = false)
    private int arrivalCityId;
    @Column(name = "arrivalDateTime", nullable = false)
    private Timestamp arrivalDateTime;

    public Flight() {
    }

    public Flight(String flightNumber, String airplaneType, int departureCityId, Timestamp departureDateTime, int arrivalCityId, Timestamp arrivalDateTime) {
        this.flightNumber = flightNumber;
        this.airplaneType = airplaneType;
        this.departureCityId = departureCityId;
        this.departureDateTime = departureDateTime;
        this.arrivalCityId = arrivalCityId;
        this.arrivalDateTime = arrivalDateTime;
    }

    public Flight(int id, String flightNumber, String airplaneType, int departureCityId, Timestamp departureDateTime, int arrivalCityId, Timestamp arrivalDateTime) {
        this.id = id;
        this.flightNumber = flightNumber;
        this.airplaneType = airplaneType;
        this.departureCityId = departureCityId;
        this.departureDateTime = departureDateTime;
        this.arrivalCityId = arrivalCityId;
        this.arrivalDateTime = arrivalDateTime;
    }

     public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public int getDepartureCityId() {
        return departureCityId;
    }

    public void setDepartureCityId(int departureCityId) {
        this.departureCityId = departureCityId;
    }

    public Timestamp getDepartureDateTime() {
        return departureDateTime;
    }

    public void setDepartureDateTime(Timestamp departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public int getArrivalCityId() {
        return arrivalCityId;
    }

    public void setArrivalCityId(int arrivalCityId) {
        this.arrivalCityId = arrivalCityId;
    }

    public Timestamp getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(Timestamp arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }
}
