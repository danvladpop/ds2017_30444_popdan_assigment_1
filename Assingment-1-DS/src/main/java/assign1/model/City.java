package assign1.model;
import javax.persistence.*;


@Entity
@Table(name = "city", catalog = "flightDB")
public class City implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;
    @Column(name = "cityName", unique = true, nullable = false, length = 45)
    private String cityName;
    @Column(name = "longitude", unique = true, nullable = false)
    private Float longitude;
    @Column(name = "latitude", unique = true, nullable = false)
    private float latitude;

    public City() {
    }

    public City(String cityName, Float longitude, float latitude) {
        this.cityName = cityName;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public City(int id, String cityName, Float longitude, float latitude) {
        this.id = id;
        this.cityName = cityName;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }
}
