package assign1.controller;

import org.hibernate.cfg.Configuration;
import assign1.model.Flight;
import assign1.service.FlightService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class ListFlightsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ListFlightsServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher rd = null;


        FlightService flightService = new FlightService(new Configuration().configure().buildSessionFactory());

        Flight flight2 = flightService.getFlight(4);
        Flight flight1 = flightService.getFlight(1);

        ArrayList<Flight> flightList = new ArrayList<>();

        flightList.add(flight1);
        flightList.add(flight2);

        flightList= (ArrayList<Flight>) flightService.getAllFlights();

        if(request.getSession().getAttribute("logged")!=null){
            if(request.getSession().getAttribute("logged").toString().equals("admin")){
                rd = request.getRequestDispatcher("/listFlights.jsp");
                request.setAttribute("flightList", flightList);
                rd.forward(request, response);
            }
        }

        rd = request.getRequestDispatcher("/listFlightsUser.jsp");
        request.setAttribute("flightList", flightList);

        rd.forward(request, response);
    }

}