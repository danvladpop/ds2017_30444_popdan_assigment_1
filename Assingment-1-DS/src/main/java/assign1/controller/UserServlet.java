package assign1.controller;

import org.hibernate.cfg.Configuration;
import assign1.model.User;
import assign1.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;



public class UserServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private UserService userService;

    public UserServlet() {
        super();
        userService = new UserService(new Configuration().configure().buildSessionFactory());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        RequestDispatcher rd;

        User o = userService.add(new User(name, username, password, email));
        if (o != null) {
            rd = request.getRequestDispatcher("/success.jsp");
            request.setAttribute("user", (User) o);
        } else {
            rd = request.getRequestDispatcher("/error.jsp");
        }
        rd.forward(request, response);
    }

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher rd = null;
        if(request.getSession().getAttribute("logged")!=null){
            if(request.getSession().getAttribute("logged").toString().equals("admin")){
                rd = request.getRequestDispatcher("/admin.jsp");
                rd.forward(request, response);
            }
        }
        rd = request.getRequestDispatcher("/error.jsp");
        rd.forward(request, response);
    }

}
