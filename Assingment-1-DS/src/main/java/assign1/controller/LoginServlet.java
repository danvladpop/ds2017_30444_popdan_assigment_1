package assign1.controller;


import assign1.service.Authenticator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public LoginServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        RequestDispatcher rd = null;

        Authenticator authenticator = new Authenticator();
        String result = authenticator.authenticate(username, password);
        if(request.getSession().getAttribute("logged")!=null){
            rd = request.getRequestDispatcher("/error.jsp");
            rd.forward(request, response);
        }
        if (result.equals("admin")) {
            rd = request.getRequestDispatcher("/admin.jsp");
            if(request.getSession().getAttribute("logged")!=null){
                rd = request.getRequestDispatcher("/error.jsp");
            }
            request.getSession().setAttribute("logged","admin");
        } else if(result.equals("user")){
            rd = request.getRequestDispatcher("/user.jsp");
            request.getSession().setAttribute("logged","user");
        }
        else{
            rd = request.getRequestDispatcher("/error.jsp");
        }
        rd.forward(request, response);
    }
}