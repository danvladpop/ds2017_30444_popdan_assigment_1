package assign1.controller;

import org.hibernate.cfg.Configuration;
import assign1.model.City;
import assign1.model.Flight;
import assign1.service.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;


public class FlightServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private FlightService flightService;
    private CityService cityService;
    private TimeStampService timeStampService;
    public FlightServlet() {
        super();
        timeStampService = new TimeStampService();
        cityService = new CityService(new Configuration().configure().buildSessionFactory());
        flightService = new FlightService(new Configuration().configure().buildSessionFactory());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = request.getParameter("action");
        RequestDispatcher rd;

        rd = request.getRequestDispatcher("/error.jsp");
        if(action.equals("delete")){
            String id = request.getParameter("id");
            Flight flight = flightService.getFlight(Integer.parseInt(id));
            if(flight!=null){
                flightService.delete(flight);
                request.setAttribute("flight", flight);
                rd = request.getRequestDispatcher("/success.jsp");
            }
        }

        if(action.equals("create")){
            String flightNumber = request.getParameter("flightNumber");
            String airplaneType = request.getParameter("airplaneType");
            String departureCity = request.getParameter("departureCity");
            String departureDate = request.getParameter("departureDate");
            String arrivalCity = request.getParameter("arrivalCity");
            String arrivalDate = request.getParameter("arrivalDate");

            Flight flight = new Flight();
            flight.setArrivalDateTime(Timestamp.valueOf(arrivalDate));
            flight.setFlightNumber(flightNumber);
            flight.setAirplaneType(airplaneType);
            flight.setArrivalCityId(Integer.parseInt(arrivalCity));
            flight.setDepartureDateTime(Timestamp.valueOf(departureDate));
            flight.setDepartureCityId(Integer.parseInt(departureCity));

            flightService.add(flight);
            request.setAttribute("flight", flight);
            rd = request.getRequestDispatcher("/admin.jsp");
        }

        if(action.equals("go-edit")) {
            String id = request.getParameter("id");
            Flight flight =  flightService.getFlight(Integer.parseInt(id));
            request.setAttribute("flight",flight);
            rd = request.getRequestDispatcher("/editFlight.jsp");
        }

        if(action.equals("edit")) {
            String id = request.getParameter("id");
            String flightNumber = request.getParameter("flightNumber");
            String airplaneType = request.getParameter("airplaneType");
            String departureCity = request.getParameter("departureCity");
            String departureDate = request.getParameter("departureDate");
            String arrivalCity = request.getParameter("arrivalCity");
            String arrivalDate = request.getParameter("arrivalDate");

            Flight flight = new Flight();
            flight.setId(Integer.parseInt(id));
            flight.setArrivalDateTime(Timestamp.valueOf(arrivalDate));
            flight.setFlightNumber(flightNumber);
            flight.setAirplaneType(airplaneType);
            flight.setArrivalCityId(Integer.parseInt(arrivalCity));
            flight.setDepartureDateTime(Timestamp.valueOf(departureDate));
            flight.setDepartureCityId(Integer.parseInt(departureCity));

            Flight result =flightService.edit(flight);
            if(result!=null)
                rd = request.getRequestDispatcher("/admin.jsp");
            else
                rd = request.getRequestDispatcher("/error.jsp");
        }

        if(action.equals("localTime")) {
            String id = request.getParameter("id");
            Flight flight =  flightService.getFlight(Integer.parseInt(id));
            int departureId = flight.getDepartureCityId();


            
            int arrivalId = flight.getArrivalCityId();
            City arrivalCity = cityService.getCity(arrivalId);
            City departureCity =  cityService.getCity(departureId);

            Timestamp arrivalTimestamp = timeStampService.getTimeStampForCoordinates(arrivalCity.getLongitude(),arrivalCity.getLatitude());
            Timestamp departureTimestamp = timeStampService.getTimeStampForCoordinates(departureCity.getLongitude(),departureCity.getLatitude());
            request.setAttribute("arrivalTimestamp",arrivalTimestamp);
            request.setAttribute("departureTimestamp",departureTimestamp);
            rd = request.getRequestDispatcher("/city-timestamps.jsp");
        }
        rd.forward(request, response);
    }
}
