<%--
  Created by IntelliJ IDEA.
  User: Dan
  Date: 10/28/2017
  Time: 9:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Edit Flight</title>
</head>
<body>
<form action="FlightServlet" method="post">
    Flight number<input type="text" name="flightNumber" value="${flight.flightNumber}"> <BR>
    Airplane Type: <input type="text" name="airplaneType" value="${flight.airplaneType}"> <BR>
    Departure City : <input type="text" name="departureCity" value="${flight.departureCityId}"> <BR>
    Departure Date : <input type="text" name="departureDate" value="${flight.departureDateTime}"> <BR>
    Arrival City : <input type="text" name="arrivalCity" value="${flight.arrivalCityId}"> <BR>
    Arrival Date : <input type="text" name="arrivalDate" value="${flight.arrivalDateTime}"> <BR>
    <input type="hidden" name="action" value="edit"/>
    <input type="hidden" name="id" value="${flight.id}"/>
    <input type="submit" />
</form>
</body>
</html>
