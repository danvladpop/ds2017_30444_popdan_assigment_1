<%--
  Created by IntelliJ IDEA.
  User: Dan
  Date: 01.04.2017
  Time: 17:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
    <title>Admin page</title>
</head>
<body>
<h1>Admin home page</h1>
<p>
    Administrator operations: <br/>
</p>

<form action="ListFlightsServlet" method="get">
    <input type="submit" value="ListFlights"/>
</form>

<a href="create-flight.jsp">Add-Flight</a>

<br>
<form action="LogoutServlet" method="post">
    <input type="submit" value="Logout"/>
</form>

</body>
</html>
