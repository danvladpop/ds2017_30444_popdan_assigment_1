<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
    <title>Flight List page</title>
</head>
<body>
<h1>Flight List page</h1>


<table style="text-align: center;" border="1px" cellpadding="0" cellspacing="0">
    <thead>
    <tr>
        <th width="25px">flight Number</th>
        <th width="25px">Airplane Type</th>
        <th width="25px">Departure Time</th>
        <th width="25px">Departure City Id</th>
        <th width="25px">Arrival Time</th>
        <th width="25px">Arrival City Id</th>
        <th width="25px">Actions</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="flight" items="${flightList}">
        <tr>
            <td>${flight.flightNumber}</td>
            <td>${flight.airplaneType}</td>
            <td>${flight.departureDateTime}</td>
            <td>${flight.departureCityId}</td>
            <td>${flight.arrivalDateTime}</td>
            <td>${flight.arrivalCityId}</td>
            <td>
                <form action="FlightServlet" method="post">
                    <input type="hidden" name=id value="${flight.getId()}"/>
                    <input type="hidden" name=action value="localTime"/>
                    <input type="submit" value="Show Local Time"/>
                </form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>


<button type="button" name="back" onclick="history.back()">back</button>
</body>
</html>